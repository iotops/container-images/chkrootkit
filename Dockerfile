FROM ubuntu:latest

RUN apt update && apt install -y chkrootkit iproute2 && rm -rf /var/cache/apt

CMD [ "chkrootkit", "-r", "/target" ]

